#!/usr/bin/python

import sys
import os
import urllib.request
import xml.dom.minidom

unread = []

for account in open(os.environ['HOME'] + '/.gmailaccounts', encoding='utf-8'):
    (url, user, passwd) = account.split('|')

    auth_handler = urllib.request.HTTPBasicAuthHandler()
    auth_handler.add_password(realm='New mail feed', uri='https://mail.google.com/', user=user, passwd=passwd)
    opener = urllib.request.build_opener(auth_handler)
    urllib.request.install_opener(opener)

    request = urllib.request.urlopen(url)
    dom = xml.dom.minidom.parseString(request.read())
    count = dom.getElementsByTagName('fullcount')[0].childNodes[0].data

    unread.append(count)

# the unread list contains the unread count of each mail account
print(' / '.join(unread))
