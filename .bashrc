alias Fg='Play "Family Guy"'
alias azu='Play "Azumanga Daioh"'
alias bt="transmission-remote TjsLaptop"
alias dir_struct='less /home/tharihar/Documents/directory\ structure'
alias friends='Play Friends'
alias fuck='fsck'
#alias ls='ls --color=auto'
alias manhood='man'
alias music='ncmpcpp'
alias next='clear&&mpc next&&Info'
alias play='clear&&mpc play&&Info'
alias prev='clear&&mpc prev&&Info'
alias song_info='songinfo'
alias songinfo='clear&&mpc'
alias suvim='sudo vim'
alias uptime_log='less /var/log/Uptime.log'
alias vol='mpc volume '
alias rm='rm -vi'
alias cp='cp -vi'
alias mv='mv -vi'
alias xman='xman -notopbox'
alias youtube='clive --youtube="TjPhysicist:miriam" --ffmpeg="/usr/bin/ffmpeg -y -i %i %o"'
alias burn='bashburn'
alias tagedit='easytag'
alias go='startx >&/dev/null 2>&1'
alias updateworld='sudo pacman -Syu&&checkupgrades'
alias xclip='xclip -i -selection "clipboard"'
alias x='exit'
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
#PS1=" \w<\$(parse_git_branch)> $"
export PS1="\[\e[0;32m\]\u@\h\[\e[m\]\[\e[1;34m\]:\w\[\e[m\] \[\e[m\]\[\e[0;31m\]<\$(parse_git_branch)>\[\e[1;35m\] $ \[\e[m\]\[\e[1;37m\]"
alias vim='vim -p' #vim in tags...
export PATH=$PATH:/opt/bin/android-sdk/tools:/usr/local/bin:/home/tharihar/.Bin
export MPD_HOST=127.0.0.1
export MPD_PORT=6600
####### Semester ###########
prefix=1
year=$(date +%y)
#month=$(date +%-m)
#if [ "${month}" -ge "9" ]
#then
	month="9"
#elif [ "${month}" -ge "5" ]
#then
#	month="5"
#else
#	month="1"
#fi

export semester="${prefix}${year}${month}"
#############
export PS1="\[\e[0;31m\][${semester}] -- ${PS1}"
#export PS1="\[\e[0;31m\]┌────────────[${semester}]────────┐\n└─${PS1}" #add semester to prompt
#Login greeting -------------------------------
#if [ "$TERM" = "screen" -a ! "$SHOWED_SCREEN_MESSAGE" = "true" ]; then
#  detached_screens=`screen -list | grep Detached`
#  if [ ! -z "$detached_screens" ]; then
#    echo "+---------------------------------------+"
#    echo "| Detached screens are available:       |"
#    echo "$detached_screens"
#    echo "+---------------------------------------+"
#    echo ""
#    echo "reattach session? (type number 0 for none)."
#    read $ans
#    if [ "$ans" == "0" ]; then
#	    exec screen
#   else
#	    exec screen -r $ans
#    fi
#  else
#    exec screen 
#    echo "[ screen is activated ]"
#  fi
#  export SHOWED_SCREEN_MESSAGE="true"
#fi
#-------------------------------------------------

#needed for utf in urxvt
printf '\33]701;%s\007' en_US.UTF-8
