#!/usr/bin/perl

my ($config,$pid,$xid,$fifo,$socket,$url,$title,@cmd) = @ARGV;
if($fifo eq "") { die "No fifo"; };

#If there are no dots in the first word or more than one word is suppllied expect a phrase for google. Else go to the uri specified.
if (index(@cmd[0], '.') == -1 || scalar @cmd > 1)
{
        # Replace this with your search engine
	qx(echo "uri http://www.google.com/search?q=@cmd&btnI=I\'m+Feeling+Lucky" >> $fifo);
}
else
{
	qx(echo "uri @cmd" >> $fifo);
}

