#!/bin/bash

ind=$(expr index "$8" '.')
if [ ${ind} -eq 0 ] 
then
	uzbl-browser --uri="http://www.google.com/search?q=$8&btnI=I\'m+Feeling+Lucky"

else
	uzbl-browser --uri="$8"

fi
