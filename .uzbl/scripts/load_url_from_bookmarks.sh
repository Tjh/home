#!/bin/bash
bookmarks_file=${XDG_DATA_HOME:-$HOME/.local/share}/uzbl/bookmarks
bookmarks_html=${XDG_DATA_HOME:-$HOME/.local/share}/uzbl/bookmarks.html

[ -r "$bookmarks_file" ] || exit 1

COLORS=" -nb #303030 -nf khaki -sb #CCFFAA -sf #303030"
DMENU="dmenu -i -l 10" # vertical patch
        
goto=`cat $bookmarks_file | $DMENU $COLORS`
url=$(grep -e "${goto}" $bookmarks_html| cut -d'"' -f2)
[ -n "$url" ] && touch /tmp/uzbl_history &&echo "uri ${url}">$4
