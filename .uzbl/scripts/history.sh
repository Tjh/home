#!/bin/bash
#TODO: strip 'http://' part
file=${XDG_DATA_HOME:-$HOME/.local/share}/uzbl/history
[ -d `dirname $file` ] || exit 1
#if 0 then im not supposed to put the url into history
if [ -f /tmp/uzbl_history ]
then
	#remove this file so it will not exist (i.e. all history will be written) until this file gets created again
	rm -f /tmp/uzbl_history
elif [ "$7" != "Google" -a "$7" != "" ] 
then
	echo "$8 [ $7 ] $6" >> $file
fi
