#r
#r
#r
#zsh stuff
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt AUTO_CD
setopt CORRECT
setopt CORRECT_ALL
unsetopt BEEP
autoload -U compinit promptinit
compinit promptinit
#autocompletion with an arrow-key driven interface
zstyle ':completion:*' menu select
setopt completealiases
setopt hist_ignore_all_dups hist_ignore_space
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line
bindkey    "^[[3~"          delete-char
bindkey    "^[3;5~"         delete-char
#aliases
alias Fg='Play "Family Guy"'
alias azu='Play "Azumanga Daioh"'
alias bt="transmission-remote TjsLaptop"
alias dir_struct='less /home/tharihar/Documents/directory\ structure'
alias friends='Play Friends'
alias ls='ls -Gl'
alias music='ncmpcpp --host 127.0.0.1 --port 6600'
alias next='clear&&mpc next&&Info'
alias play='clear&&mpc play&&Info'
alias prev='clear&&mpc prev&&Info'
alias song_info='songinfo'
alias songinfo='clear&&mpc'
alias suvim='sudo vim'
alias uptime_log='less /var/log/Uptime.log'
alias vol='mpc volume '
alias rm='rm -vi'
alias cp='cp -vi'
alias mv='mv -vi'
alias xman='xman -notopbox'
alias burn='bashburn'
alias tagedit='easytag'
alias go='startx >&/dev/null 2>&1'
alias updateworld='sudo pacman -Syu&&checkupgrades'
alias xclip='xclip -i -selection "clipboard"'
alias x='exit'
export MPD_HOST=127.0.0.1
export MPD_PORT=6600
#load colors
autoload -U colors && colors

parse_git_branch() {
  (git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')&&return
  
}

#PS1=" \w<\$(parse_git_branch)> $"
#export PS1="$(print '\[\e[0;32m\]\u@\h\[\e[m\]\[\e[1;34m\]:\w\[\e[m\] \[\e[m\]\[\e[0;31m\]<\$(parse_git_branch)>\[\e[1;35m\] $ \[\e[m\]\[\e[1;37m\]')"
alias vim='vim -p' #vim in tags...
export PATH=$PATH:/opt/bin/android-sdk/tools:/usr/local/bin:/home/tharihar/.Bin:/usr/local/texlive/2011/bin/amd64-freebsd

PROMPT="%{$fg[green]%}%n@%m:%{$fg[blue]%}%~ %{$fg[red]%}<$(parse_git_branch)> %{$fg[magenta]%}%# %{$reset_color%}"

#needed for utf in urxvt
printf '\33]701;%s\007' en_US.UTF-8
#startx &
