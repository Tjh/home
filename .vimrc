:filetype plugin on
:set backspace=indent,eol,start
:set nobomb
:filetype indent on
:set grepprg=grep\ -nH\ $*
:autocmd filetype tex source ~/.vim/auctex.vim
:set nobackup
:syn on
:set termencoding=utf-8
:set encoding=utf-8
:set spl=en_gb spell
:nmap q :q!
:set spellfile=~/.vim.words.add
:set spell
:nmap T :TlistOpen<CR>
:colorscheme blacksea
:vmap y :!xclip -i -selection "clipboard"<CR>:undo<CR>
:nmap o :NERDTree<CR>
:inoremap <C-x>o <C-x>s
:nmap M K
:nmap P "*p
:nmap dl d$s
:nmap d# dG
:nmap " y<up>p$
:nmap g<C-g> :!texcount "%" 
:filetype plugin indent on
:set incsearch
:set grepprg=grep\ -nH\ $*
:set laststatus=2
:set statusline=%t\ %=%y\ [Line:\ %l,\ Col:\ %c]
if has("multi_byte")
     set encoding=utf-8
     setglobal fileencoding=utf-8
     set bomb
     set termencoding=utf-8
     set fileencodings=utf-8
else
     echoerr "Sorry, this version of (g)vim was not compiled with +multi_byte"
endif


if has("autocmd")
    au BufReadPost *.rkt,*.rktl set filetype=scheme
endif
